import java.util.PriorityQueue;
import java.util.Comparator;
import java.lang.Integer;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class NodeRecord1
{
		int node;
		List<Integer> connection = new ArrayList<Integer>();
		int costSoFar;
		int estimatedTotalCost;
		
		public NodeRecord1(int n,List<Integer> l,int co,int etc)
		{
			node = n;
			connection = new ArrayList<Integer>(l);
			costSoFar = co;
			estimatedTotalCost = etc;
		}
		
		public int getCostSoFar()
		{
			return costSoFar;
		}
		
		public int getNode()
		{
			return node;
		}
		
		public int getEstimatedTotalCost()
		{
			return estimatedTotalCost;
		}
		
		public List<Integer> getConnection()
		{
			return connection;
		}
		
		public void setEstimatedTotalCost(int etc)
		{
			estimatedTotalCost = etc;
		}
		
}

class PathCostComparator1 implements Comparator<NodeRecord1>
{
    public int compare(NodeRecord1 x, NodeRecord1 y)
    {
        if (x.getEstimatedTotalCost() < y.getEstimatedTotalCost())
        {
            return -1;
        }
        if (x.getEstimatedTotalCost() > y.getEstimatedTotalCost())
        {
            return 1;
        }
        return 0;
    }
}


public class Astar {

	static Comparator<NodeRecord1> comparator = new PathCostComparator1();
	static PriorityQueue<NodeRecord1> open = new PriorityQueue<NodeRecord1>(10,comparator);
	static PriorityQueue<NodeRecord1> closed = new PriorityQueue<NodeRecord1>(10,comparator);
	
	public static NodeRecord1 existsInList(PriorityQueue<NodeRecord1> list, int node)
	{
		NodeRecord1 temp = null;
		for(Object o: list.toArray())
		{
			temp = (NodeRecord1)o;
			if(node == temp.getNode())	return temp;
		}
		return null;
	}
	
	public static void findPath(Graph g, int gSize, int startNode, int goalNode) throws Exception
	{
		int newCost;
		NodeRecord1 current,temp, endNodeRecord;
		List<Integer> f = new ArrayList<Integer>();
		List<Integer> connectionList;
		
		startNode--;
		goalNode--;
		current = null;
		f.add(startNode);
		NodeRecord1 startRecord = new NodeRecord1(startNode,f,0,g.euclideanDistance(startNode,goalNode));
		
		open.add(startRecord);
		
		while( !open.isEmpty() )
		{
			current = open.poll();
			
			if(current.getNode() == goalNode)	break;
			
			newCost = 0;
			
			for(int i=0; i < gSize ; i++)
			{
				if(i==current.getNode()) continue;					
				connectionList = new ArrayList<Integer>(current.getConnection());
				
				if(g.paths[current.getNode()][i] != -1)
				{
					newCost = current.getCostSoFar() + g.paths[current.getNode()][i];
					
					temp = existsInList(closed, i);				// Is it already present in closed list?
					if( temp != null )							// If it is
					{
						if( newCost >= temp.getCostSoFar())	continue;
						closed.remove(temp);
					}
					
					else
					{
						temp = existsInList(open, i);			// Is it already present in open list?
				
						if( temp != null )						// If it is present in Open List
						{
							if(newCost >= temp.getCostSoFar())	continue;
							open.remove(temp);
						}
						connectionList.add(i);
						endNodeRecord = new NodeRecord1(i, connectionList, newCost, newCost+g.euclideanDistance(i,goalNode));
						open.add(endNodeRecord);
	//					System.out.println(current.getNode()+" "+endNodeRecord.getNode()+" "+endNodeRecord.getCostSoFar() );
					}
				}
				closed.add(current);
			}
			//System.out.println(open.size());
		}
		
		if( current.getNode() != goalNode)
			System.out.println("Goal Not found!"+open.size());
		else
		{
			System.out.println("Goal found!"+current.getCostSoFar());
			Iterator<Integer> it = current.getConnection().iterator();
			System.out.println(it.hasNext());
			while(it.hasNext())
			{
				int temp1 = it.next();
				System.out.print(" "+temp1);
			}
		}
	}
	
	public static void main(String[] args) throws Exception 
	{
		int g1Size = 28;								// Real world graph
		Graph g1 = new Graph(g1Size);
		g1.extractGraph("graph1.txt");
		
		int g2Size = 5000;								// Generated Graph
		Graph g2 = new Graph(g2Size);
		g2.extractGraph("graph2.txt");
		
		findPath(g1,g1Size,1,24);
	}
}