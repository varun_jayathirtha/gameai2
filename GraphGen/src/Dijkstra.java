import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Comparator;

class NodeRecord
{
		int node;
		List<Integer> connection = new ArrayList<Integer>();
		int costSoFar;
		
		public NodeRecord(int n,List<Integer> l,int co)
		{
			node = n;
			connection = new ArrayList<Integer>(l);
			costSoFar = co;
		}
		
		public int getCostSoFar()
		{
			return costSoFar;
		}
		
		public int getNode()
		{
			return node;
		}
		
		public List<Integer> getConnection()
		{
			return connection;
		}
}

class PathCostComparator implements Comparator<NodeRecord>
{
    public int compare(NodeRecord x, NodeRecord y)
    {
        if (x.getCostSoFar() < y.getCostSoFar())
        {
            return -1;
        }
        if (x.getCostSoFar() > y.getCostSoFar())
        {
            return 1;
        }
        return 0;
    }
}


public class Dijkstra {

	static Comparator<NodeRecord> comparator = new PathCostComparator();
	static PriorityQueue<NodeRecord> open = new PriorityQueue<NodeRecord>(10,comparator);
	static PriorityQueue<NodeRecord> closed = new PriorityQueue<NodeRecord>(10,comparator);
	
	public static NodeRecord existsInList(PriorityQueue<NodeRecord> list, int node)
	{
		NodeRecord temp = null;
		for(Object o: list.toArray())
		{
			temp = (NodeRecord)o;
			if(node == temp.getNode())	return temp;
		}
		return null;
	}
	
	public static void findPath(Graph g, int gSize, int startNode, int goalNode) throws Exception
	{
		int newCost;
		List<Integer> f = new ArrayList<Integer>();
		List<Integer> connectionList;
		
		startNode--;
		goalNode--;
		
		f.add(startNode);
		NodeRecord startRecord = new NodeRecord(startNode,f,0);
		NodeRecord current,temp, endNodeRecord;
		
		open.add(startRecord);
		current = null;
		
		while( !open.isEmpty() )
		{
			boolean replace;
			
			replace = false;
			current = open.poll();
			newCost = 0;
			if(current.getNode() == goalNode)	break;
			
			for(int i=0; i < gSize ; i++)
			{
				if(i==current.getNode()) continue;
				connectionList = new ArrayList<Integer>(current.getConnection());
				
				if(g.paths[current.getNode()][i] != -1)
				{
					if( existsInList(closed, i) != null )	continue;
					temp = existsInList(open, i);
					if( temp != null )
					{
						newCost = current.getCostSoFar() + g.paths[current.getNode()][i];
						if(newCost >= temp.getCostSoFar())	continue;
						replace = true;
						open.remove(temp);
					}
					connectionList.add(i);
					endNodeRecord = new NodeRecord(i,connectionList,current.getCostSoFar()+g.paths[current.getNode()][i]);
					open.add(endNodeRecord);
//					System.out.println(current.getNode()+" "+endNodeRecord.getNode()+" "+endNodeRecord.getCostSoFar() );
				}
				closed.add(current);
			}
		}
		
		if( current.getNode() != goalNode)
			System.out.println("Goal Not found!"+open.size());
		else
		{
			System.out.println("Goal found!"+current.getCostSoFar());
			Iterator<Integer> it = current.getConnection().iterator();
			System.out.println(it.hasNext());
			while(it.hasNext())
			{
				int temp1 = it.next();
				System.out.print(" "+temp1);
			}
		}
	}
	
	public static void main(String[] args) throws Exception 
	{
		int g1Size = 28;								// Real world graph
		Graph g1 = new Graph(g1Size);
		g1.extractGraph("graph1.txt");
		
		int g2Size = 5000;								// Generated Graph
		Graph g2 = new Graph(g2Size);
		g2.extractGraph("graph2.txt");
		
		findPath(g1,g1Size,1,24);				// 1827, 637
		
	}

}
