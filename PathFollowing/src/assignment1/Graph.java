package assignment1;
import java.io.*;
import java.util.Random;

public class Graph {
	public int points[][];
	public int paths[][]; 
	private int numberOfNodes;
	private long average;
	/**
	 * @param args
	 */
	public Graph(int NodeCount)
	{
		numberOfNodes = NodeCount;
		paths = new int[numberOfNodes][numberOfNodes];
		points = new int[numberOfNodes][2];
	}
	
	// Graph Generation Functions
	
	public boolean closeToOtherNodes(int x, int y)
	{
		for(int i=0;i<numberOfNodes;i++)
		{
			if(points[i][0] == -1)
				return false;
			if( (Math.abs(points[i][0]-x) <= 5) || (Math.abs(points[i][1]-y) <= 5) )
			{
				return true;
			}
		}
		return false;
	}
	
	public void generateGraph(String FileName, int nodeCount) throws Exception 
	{
		numberOfNodes = nodeCount;
		Random generator = new Random();
		double rand;
		int count = 0;
		int range = (int)(Math.sqrt(numberOfNodes)*10);
		File f = new File(FileName);
		PrintStream fout = new PrintStream( new FileOutputStream(f) );
		
		points = new int[numberOfNodes][2];
		
		for(int i=0;i<numberOfNodes;i++)
			points[i][0] = points[i][1] = -1;
		
		for(int i=0;i<numberOfNodes;i++)
		{
			int x = generator.nextInt(numberOfNodes*10);
			int y = generator.nextInt(numberOfNodes*10);
			
			if( closeToOtherNodes(x, y) )
			{
				i--;
				try {
				    Thread.sleep(40);                 
				} catch(InterruptedException ex) {
				  Thread.currentThread().interrupt();
				}
				continue;
			}
			points[i][0] = x;
			points[i][1] = y;
			fout.println(x+" "+y);
			
		}
		
		for(int i=0;i<numberOfNodes;i++)
		{
			int x = generator.nextInt(numberOfNodes);
			
			for(int j=0;j<numberOfNodes;j++)
			{
				rand = Math.random() - Math.random(); 
				if( ((rand < 0.0005) && (rand > -0.0005)) || x==j )
				{
					count++;
					paths[i][j] = heuristic(1,i,j);
					fout.println(i+" "+j+" "+paths[i][j]);
				}
			}
		}
		fout.close();
		System.out.println(count+" "+(count/(numberOfNodes*numberOfNodes)));
	}
	
	// Graph Extraction Function
	
	public void extractGraph(String FileName) throws Exception 
	{
		int count = 0,count1 = 0, i, j;
		String line;
		String words[];
		
		for(i=0;i<numberOfNodes;i++)
			for(j=0;j<numberOfNodes;j++)
				paths[i][j] = -1;
		
		i = 0;
		
		FileReader fileReader = new FileReader(FileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

	    while((line = bufferedReader.readLine()) != null) 
	    {
	    	words = line.split(" ");
    		if(count<numberOfNodes)
		    {
    			points[i][0] = Integer.parseInt(words[0]);
    			points[i][1] = Integer.parseInt(words[1]);
    			i++;
	    	}
	    	else
	    	{
	    		if(Integer.parseInt(words[2]) == 0 || (Integer.parseInt(words[0])==Integer.parseInt(words[1])))
	    			continue;
	    		paths[Integer.parseInt(words[0])][Integer.parseInt(words[1])] = Integer.parseInt(words[2]);
	    		count1++;
	    		average += Integer.parseInt(words[2]);
	    	}
		    count++;
		}
	    average /= count1;
	    bufferedReader.close();
	}
	
	// Other functions
	
	public int heuristic(int type, int i, int j)
	{
		int x1 = points[i][0];
		int y1 = points[i][1];
		int x2 = points[j][0];
		int y2 = points[j][1];
		
		if(type == 1)				//Euclidean Distance
			return (int) Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );	
		
		else if(type == 2)			// Manhattan Distance
			return Math.abs(x1-x2) + Math.abs(y1-y2);
		
		return (int)average;
	}
	
	public int nearestNode(int x, int y)
	{
		int index = 0;
		double dist = 1000000;
		
		for(int i=0;i<numberOfNodes;i++)
		{
			int x1 = points[i][0];
			int y1 = points[i][1];
			
			double temp = Math.sqrt( (x1-x)*(x1-x) + (y1-y)*(y1-y) );
			if( dist > temp )
			{
				index = i;
				dist = temp;
			}
		}
		
		return index;
	}
	
	public void printPathValue(int x,int y)
	{
		System.out.println(paths[x][y]);
	}
}
