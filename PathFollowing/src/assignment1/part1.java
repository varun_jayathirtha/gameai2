package assignment1;

import processing.core.PApplet;
import java.util.*;

public class part1 extends PApplet 
{
	int xMax, zMax;
	int gSize;
	Graph g;
	Astar pathFinder;
	Queue<Integer> q = new LinkedList<Integer>(); 
	Iterator<Integer> nodesList;
	Queue<Integer> goalQueue = new LinkedList<Integer>();
	// Kinematic 
	double x, z;
	double vx, vz, vr;
	double r;
		
	double vMax,vMaxSq, rMax;
	// steering output
	double ax, az;
	double ar;
	
	final double pi = Math.PI;
	
	double targetX, targetZ;
	boolean started;
	
	final double radiusSat = 20, radiusDeccl = 400;
	final double timeToTargetVelocity = 500;
	
	int counter,target;
	int traceLength;
	long time1,time;
		
	List trace = new ArrayList();
	  
	public void setup() {
	}
	
	public void settings() 
	{
		started = false;
		targetX = 0;
		targetZ = 0;
		xMax = 800;
		zMax = 700;
		
		gSize = 24;
		g = new Graph(gSize);
		try{
			g.extractGraph("graph3.txt");
		}
		catch (Exception e) {
			System.err.println("Exception: " + e.getMessage());
		}
		
		x = (g.points[16][0])/4;
		z = zMax - ((g.points[16][1])/4);
		r = 0;
		vr = vz = vx = 0.00;
		
		vMax = 0.02;
		vMaxSq = Math.sqrt(vMax);
		rMax = 0.025;
		counter = 0;
		traceLength = 50;
		time1 = time = System.currentTimeMillis();
	     
	    
		
		nodesList = q.iterator();
		
		size(xMax,zMax);
	}
		
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		fill(0);
		ellipse(xC,zC,13,13);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		fill(color);
		ellipse(xC,zC,13,13);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation()
	{
		double r2, rot=0;
		if((vz*vz + vx*vx) > 0)
		{
			r2 = Math.atan2(vz,vx);
			rot = r2 - r;
			
			if(rot > pi)
				rot -= 2*pi;
			else if(rot < -pi)
				rot += 2*pi;
			
			if(Math.abs(rot)<rMax)
			{
				r = r2;
				rot = 0;
			}
			else
			{
				if( Math.abs(rot) > rMax)
					rot =  (rot > 0)? rMax : -rMax;
				r += rot;
			}
		}
	}
	
	// Gradual Stop
	
	public void seek(double tX, double tZ, int mode)
	{

		double dx = tX - x;
		double dz = tZ - z;
		double dist = Math.sqrt((dx*dx) + (dz*dz));
		double velX, velZ;
		
		if(mode == 0)
		{
			double goalSpeed;
			if(dist < radiusSat)
			{
				ax = -vx/timeToTargetVelocity;
				az = -vz/timeToTargetVelocity;
				setNewOrientation();
	
				return;
			}
			if(dist > radiusDeccl)
			{
				goalSpeed = vMax;
			}
			else
			{
				goalSpeed = vMax*dist/radiusDeccl;
			}
			
			velX = dx/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed); 
			velZ = dz/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed);
			
			ax = (velX - vx)/timeToTargetVelocity;
			az = (velZ - vz)/timeToTargetVelocity;
			setNewOrientation();
		}
		else
		{
			if(dist < 4)
			{
				setNewOrientation();
				vz = vx = az = ax = 0;
			}
			else
			{
				velX = dx/Math.sqrt(dx*dx + dz*dz)*vMaxSq; 
				velZ = dz/Math.sqrt(dx*dx + dz*dz)*vMaxSq;
				
				ax = (velX - vx)/timeToTargetVelocity;
				az = (velZ - vz)/timeToTargetVelocity;
				setNewOrientation();
			}
		}

	}
	
	public void draw() 
	{
		noStroke();
		background(255);
		  
		int x, z;
			
		for(int i=0;i<gSize;i++)
		{
			x = g.points[i][0]/4;
			z = zMax - g.points[i][1]/4;
			fill(0);
			ellipse(x,z,5,5);
			fill(255);
			ellipse(x,z,4,4);
		}
		
		shadow();
		shapePrint();
		update();
	}
	
	public void update()
	{
		long newTime = System.currentTimeMillis();
		long diff = newTime - time;
		int mode = 0;
		if(started)
		{
			double dist = Math.sqrt( (targetX-x)*(targetX-x) + (targetZ-z)*(targetZ-z) );
			if(dist < 4)
			{
				Integer index = q.poll();
				if(index != null)
				{
					targetX = g.points[index][0]/4;
					targetZ = zMax - g.points[index][1]/4;
				}
				else
				{
					int current = g.nearestNode((int)(x*4), (int)(4*(zMax-z))) + 1;
					Integer goal = goalQueue.poll();
					List<Integer> path = null;
					Iterator<Integer> it = null;
					
					if(goal != null )
					{
						try{
							path = (new Astar()).findPath(g,gSize,current,goal,1);
						}
						catch(Exception e)
						{
							System.err.println("Exception: "+ e.getMessage());
						}
						it = path.iterator();
						
						while(it.hasNext())
						{
							Integer temp = it.next(); 
							q.add(temp);
						}
						Integer temp = q.poll();
						targetX = g.points[temp][0]/4;
						targetZ = zMax - g.points[temp][1]/4;
					}
				}
				
			}
			
			seek(targetX,targetZ,mode);					// Set Seek mode here 	
														// 0 - Gradual acceleration, Gradual decceleration
														// 1 - Gradual acceleration, Instant decceleration(sudden stop)
			x += vx*diff + 0.5*ax*diff*diff;
			z += vz*diff + 0.5*az*diff*diff;
			r += vr*diff;
			
			vx += ax*diff;
			vz += az*diff;
			
		}	
		newTime = System.currentTimeMillis();
		
		if(newTime - time1 > 350)
		{
			if(trace.size() > traceLength)
			{
				trace.remove(0);
				trace.remove(0);
				trace.remove(0);
			}
			trace.add(x);
			trace.add(z);
			trace.add(r);
			time1 = newTime;
		}
		time = newTime;
	}	
	
	public void mousePressed()
	{
		int current = g.nearestNode((int)(x*4), (int)(4*(zMax-z))) + 1;
		int goal = g.nearestNode(mouseX*4, 4*(zMax-mouseY)) + 1;
		List<Integer> path = null;
		Iterator<Integer> it = null;
		
		if(started)
		{
			goalQueue.add(goal);
		}
		else
		{
			try{
				path = (new Astar()).findPath(g,gSize,current,goal,1);
			}
			catch(Exception e)
			{
				System.err.println("Exception: "+ e.getMessage());
			}
			it = path.iterator();
			
			while(it.hasNext())
			{
				Integer temp = it.next(); 
				q.add(temp);
			}
			Integer temp = q.poll();
			targetX = g.points[temp][0]/4;
			targetZ = zMax - g.points[temp][1]/4;
		}
		started = true;
	}	
	
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.part1.class.getName() });
	}
}


